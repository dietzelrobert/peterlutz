import translationsDe from "./de.json";
import translationsEn from "./en.json";

export default {
    translationsDe,
    translationsEn
};