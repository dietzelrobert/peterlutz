/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import vuetify from './plugins/vuetify'
import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';
import translate from "../public/lang/index.js";
import VueMeta from "vue-meta";

Vue.use(VueI18Next);
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
});

Vue.config.productionTip = false
const i18n = new VueI18Next(i18next);

router.beforeEach((to, from, next) => {
  let language = to.params.lang;
  if (!language) {
    language = "de";
  }
  i18next.changeLanguage(language);
  next();
});

// Locale Settings
i18next.init({
  lng: "de",
  resources: {
      en: {translation: translate.translationsEn},
      de: {translation: translate.translationsDe}
  }
});

new Vue({
  i18n,
  router,
  vuetify,
  data () {
    return {
      loginState: false,
      loggedInUserDisplayName: ""
    }
  },
  render: h => h(App)
}).$mount('#app')
