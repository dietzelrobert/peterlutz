import Vue from "vue";
import Router from "vue-router";
import Offer from "./views/offer.vue";
import About from "./views/about.vue";
import Clients from "./views/customerprojects.vue";
import Contact from "./views/contact.vue";
import Projects from "./views/projects.vue";
import Benefit from "./views/benefit.vue";
import Home from "./views/home.vue";
import Imprint from "./views/imprint.vue";
import Balgrist from "./views/clientPages/balgrist.vue";
import EPFL from "./views/clientPages/epfl.vue";
import Unibe from "./views/clientPages/unibe.vue";
import PNF from "./views/PageNotFound.vue";
import ETHZ from "./views/clientPages/ethz.vue";
import UZH from "./views/clientPages/uzh.vue";
import Khalifa from "./views/clientPages/khalifa.vue";
import MD from "./views/clientPages/MD.vue";


Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            redirect: "de"
        },
        { 
            path: "/:lang",
            component: {
                render (c) {
                    return c("router-view");
                }
            },
            children: [
                {
                    path: "/",
                    name: "Start",
                    component: Home,
                },
                {
                    path: "benefit",
                    name: "Ihr Nutzen",
                    component: Benefit
        
                },
                {
                    path: "offer",
                    name: "Angebot",
                    component: Offer
                },
                {
                    path: "about",
                    name: "Firma",
                    component: About
                },
                {
                    path: "projects",
                    name: "Projekte",
                    component: Projects
                },
                {
                    path: "customerprojects",
                    name: "Kundenprojekte",
                    component: Clients
                },
                {
                    path: "contact",
                    name: "Kontakt",
                    component: Contact
                },
                {
                    path: "imprint",
                    name: "Impressum",
                    component: Imprint
                },
                {
                    path: "balgrist",
                    name: "Balgrist",
                    component: Balgrist
                },
                {
                    path: "epfl",
                    name: "EPFL",
                    component: EPFL
                },
                {
                    path: "unibe",
                    name: "Uni Bern",
                    component: Unibe
                },
                {
                    path: "ethz",
                    name: "ETH Zürich",
                    component: ETHZ
                },
                {
                    path: "uzh",
                    name: "Universität Zürich",
                    component: UZH
                },
                {
                    path: "md",
                    name: "Mc Donalds",
                    component: MD
                },
                {
                    path: "khalifa",
                    name: "Universität Khalifa",
                    component: Khalifa
                },
                {
                    path: '404',
                    name: '404',
                    component: PNF
                }

            ]
        }
        
    ],
    scrollBehavior () {
        return { x: 0, y: 0 }
      }
});